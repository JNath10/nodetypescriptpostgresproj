const { exec } = require("child_process");

const dockerImageName = `keeper-api`;


var buildDocker = async()=>{
    await execChildProsesAsync(`sudo docker rm $(sudo docker stop $(sudo docker ps -a -q --filter ancestor='${dockerImageName}' --format="{{.ID}}"))`);
    await execChildProsesAsync(`sudo docker build -t ${dockerImageName} .`);
    await execChildProsesAsync(`sudo docker run -p 8080:8080 --detach --name keeperApiInstance1 ${dockerImageName}`);
}

var execChildProsesAsync = async (command) => {
    return new Promise((resolve, rejects) => {
        exec(command, (error, stdout, stderr) => {
            if (error) {
                console.log(`error: ${error.message}`);
            //    rejects(`error: ${error.message}`)
            }
            if (stderr) {
                console.log(`stderr: ${stderr}`);
                // rejects(`stderr: ${stderr}`)
            }
            console.log(`stdout: ${stdout}`);
            resolve(`stdout: ${stdout}`)
        });
    })

}


buildDocker();