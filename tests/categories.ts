
import * as categoriesCtrl from "../src/modules/categories/controller/categories_ctrl";
import { expect } from "chai";

describe('categories', () => {
  it('getCategories', async() => {
    const result =await categoriesCtrl.getCategories();
    expect(result.categories.length).not('0');
  });
});