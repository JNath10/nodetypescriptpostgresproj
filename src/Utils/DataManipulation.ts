import { NumberConverter } from "./Numbers";

export function getRandomDataFromArr(arr: any[]) {
    return arr[NumberConverter.getRandomIntBetweenTwoValues(0, arr.length)];
}

export function removeDuplicateValueFromArr(arr: any[]) {
    return arr.filter((elem, index, self) => {
        return index === self.indexOf(elem);
    })
}