export function getParamAsString(params: any, key: string): string {
    return params[key];
}

export function getParamAsInt(params: any, key: string): number {
    return parseInt(params[key],10);
}

export function getParamAsStringArray(body: any): string[] {
    return body;
}


export function getStringArrayComa(params: any, key: string): string[] {
    let arr = [];
    if (params[key]) {
        arr = params[key].split(',');
    }

    return arr;
}