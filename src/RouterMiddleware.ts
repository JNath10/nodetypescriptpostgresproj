import { ResStatus } from "./Response";

import assertNever from "assert-never";
import * as bodyParser from "body-parser";
import * as express from "express";
import * as logger from "./logger";
import { MessageName, messageValidator } from "./MessageValidation";

// import { elasticSearchPostEvent } from "./api_metrics/elasticsearch";
// import { RequestEvent } from "./api_metrics/RequestEvent";

/**
 * Can be thrown by handlers. Example:
 *
 *     throw new StatusError(404);
 */
export class StatusError extends Error {
    // TODO A better method for custom Error types?
    // <https://gist.github.com/justmoon/15511f92e5216fa2624b#gistcomment-1928632>

    constructor(public readonly statusCode: number, public readonly body?: any) {
        super("Handler returned HTTP status code " + statusCode);
    }
}

export class ObjectNotFound extends StatusError {
    constructor(message: string = "Object not found") {
        super(404, { error: message });
    }
}

export interface ValidationError {
    dataPath: string;
    message: string;
}

/**
 * Can be thrown by handlers.
 */
export class ReqBodyValidationError extends Error {
    constructor(public readonly errors: ValidationError[]) {
        super("Handler returned Request body validation errors");
    }
}

export class ResponseReceived {
    protected dummy: ResponseReceived;
}

export interface Router<C, U, R> {
    GET<Rsp>(
        url: string,
        doc: string,
        rspType: MessageName | void,
        handler: ((context: C, params: any, user: U | null, raw: R) => Promise<Rsp>)): void;

    POST<Req, Rsp>(
        url: string,
        doc: string,
        reqType: MessageName | void,
        rspType: MessageName | void,
        handler: ((context: C, params: any, user: U | null, req: Req, raw: R) => Promise<Rsp>)): void;

    PUT<Req, Rsp>(
        url: string,
        doc: string,
        reqType: MessageName | void,
        rspType: MessageName | void,
        handler: ((context: C, params: any, user: U | null, req: Req, raw: R) => Promise<Rsp>)): void;

    PATCH<Req, Rsp>(
        url: string,
        doc: string,
        reqType: MessageName | void,
        rspType: MessageName | void,
        handler: ((context: C, params: any, user: U | null, req: Req, raw: R) => Promise<Rsp>)): void;

    DELETE(
        url: string,
        doc: string,
        handler: ((context: C, params: any, user: U | null, raw: R) => Promise<void>)): void;

    /**
     * The `respond` function should be called exactly one time
     */
    GET_streaming(
        url: string,
        doc: string,
        handler: (
            context: C,
            params: any,
            user: U | null,
            respond: (writer: (write: (data: string) => Promise<void>) => Promise<void>) => Promise<ResponseReceived>
        ) => Promise<ResponseReceived>): void;
}

type HttpMethod = "GET" | "POST" | "PUT" | "PATCH" | "DELETE";

export const enum NoAuth {
    ANONYMOUS,
    EXPIRED,
    BLOCKED,
    IP_ADDRESS_RESTRICTED
}

export interface RequestContextFactory<RequestContext> {
    onRequestBegin: (method: string, url: string, req: express.Request) => RequestContext;
    onRequestEnd: (reqCtx: RequestContext, statusCode: number) => void;
}

export const enum DisplayErrorDecision {
    /**
     * Show the full error message in the HTTP response body. This should only
     * be used during development. This is because it may reveal internal
     * details about our code (such as full stack traces) that could be
     * beneficial to an attacker
     */
    SHOW_FULL_ERROR,

    /**
     * Return a generic "Internal Server Error" message in the response body.
     * This should be used during production.
     */
    HIDE_ERROR
}

export class RouterExpress<Ctx, CtxImpl extends Ctx, User> implements Router<Ctx, User, express.Request> {
    constructor(
        private readonly app: express.Express,
        public requestContextCallbacks: RequestContextFactory<CtxImpl>,
        private auth: (context: Ctx, req: express.Request) => Promise<User | NoAuth>,
        public errorCallback: (req: express.Request, user: User | null, err: any) => DisplayErrorDecision) {
    }

    private sendServerErrorResponse(req: express.Request, res: express.Response, user: User | null, err: any): void {
        const decision = this.errorCallback(req, user, err);
        switch (decision) {
            case DisplayErrorDecision.HIDE_ERROR:
                res.status(500).contentType("text/plain").send("500 Internal Server Error\n");
                break;
            case DisplayErrorDecision.SHOW_FULL_ERROR:
                let errorString = "500 Internal Server Error\n\n";
                errorString += `Unhandled Exception: ${req.url}\n`;
                if (err.stack) {
                    errorString += `${err.stack}\n`;
                } else {
                    errorString += `${err}\n`;
                }

                res.status(500).contentType("text/plain").send(errorString);
                break;
            default:
                return assertNever(decision);
        }
    }

    public rawRequest(method: HttpMethod, url: string, middleware: express.RequestHandler[], handler: (context: Ctx, req: express.Request) => Promise<(res: express.Response) => void>): void {
        let methodFn: Function;
        switch (method) {
            case "GET":
                methodFn = this.app.get;
                break;
            case "POST":
                methodFn = this.app.post;
                break;
            case "PUT":
                methodFn = this.app.put;
                break;
            case "PATCH":
                methodFn = this.app.patch;
                break;
            case "DELETE":
                methodFn = this.app.delete;
                break;
            default:
                methodFn = assertNever(method);
        }

        let args: any[] = [];
        args.push(url);
        args = args.concat(middleware);
        args.push(async (req: express.Request, res: express.Response) => {
            let ctx: CtxImpl | null = null;
            try {
                ctx = this.requestContextCallbacks.onRequestBegin(method, url, req);
                const responder = await handler(ctx, req);
                responder(res);
            } catch (e) {
                if (e instanceof StatusError) {
                    if (typeof e.body === "object") {
                        res.status(e.statusCode).contentType("application/json").send(JSON.stringify(e.body));
                        logger.commError(req, res, null, e.body);
                    } else {
                        res.status(e.statusCode).send();
                        logger.commError(req, res, null, null);
                    }
                    return;
                }

                this.sendServerErrorResponse(req, res, null, e);
                logger.commError(req, res, null, "500 Internal Server Error");
                return;
            } finally {
                if (ctx !== null) {
                    this.requestContextCallbacks.onRequestEnd(ctx, res.statusCode);
                }
            }
        });

        methodFn.apply(this.app, args);
    }

    private jsonParser = bodyParser.json({
        // A large limit is needed for photo file uploads
        // TODO Maybe make this configurable for each endpoint
        limit: "20mb"
    });

    /**
     * Set to false in production for better performance
     */
    public debugValidateResponses: boolean = true;

    public GET<Rsp>(
        url: string,
        doc: string,
        rspType: MessageName | void,
        handler: ((context: Ctx, params: any, user: User | null, raw: express.Request) => Promise<Rsp>)): void {
        // Ignore parameter
        doc; // tslint:disable-line:no-unused-expression
        this.app.get(url, (req, res) => {
            const handlerWrapper = (context: Ctx, params: any, user: User | null, _req: void, raw: express.Request): Promise<Rsp> => {
                return handler(context, params, user, raw);
            };

            this.genericHandler<void, Rsp>("GET", url, undefined, rspType, req, res, handlerWrapper);
        });
    }

    public POST<Req, Rsp>(
        url: string,
        doc: string,
        reqType: MessageName | void,
        rspType: MessageName | void,
        handler: ((context: Ctx, params: any, user: User | null, req: Req, raw: express.Request) => Promise<Rsp>)): void {
        // Ignore parameter
        doc; // tslint:disable-line:no-unused-expression
        this.app.post(url, this.jsonParser, (req, res) => {
            this.genericHandler<Req, Rsp>("POST", url, reqType, rspType, req, res, handler);
        });
    }

    public PUT<Req, Rsp>(
        url: string,
        doc: string,
        reqType: MessageName | void,
        rspType: MessageName | void,
        handler: ((context: Ctx, params: any, user: User | null, req: Req, raw: express.Request) => Promise<Rsp>)): void {
        // Ignore parameter
        doc; // tslint:disable-line:no-unused-expression
        this.app.put(url, this.jsonParser, (req, res) => {
            this.genericHandler<Req, Rsp>("PUT", url, reqType, rspType, req, res, handler);
        });
    }

    public PATCH<Req, Rsp>(
        url: string,
        doc: string,
        reqType: MessageName | void,
        rspType: MessageName | void,
        handler: ((context: Ctx, params: any, user: User | null, req: Req, raw: express.Request) => Promise<Rsp>)): void {
        // Ignore parameter
        doc; // tslint:disable-line:no-unused-expression
        this.app.patch(url, this.jsonParser, (req, res) => {
            this.genericHandler<Req, Rsp>("PATCH", url, reqType, rspType, req, res, handler);
        });
    }

    public DELETE(
        url: string,
        doc: string,
        handler: ((context: Ctx, params: any, user: User | null, raw: express.Request) => Promise<void>)): void {
        // Ignore parameter
        doc; // tslint:disable-line:no-unused-expression
        this.app.delete(url, this.jsonParser, (req, res) => {
            const handlerWrapper = (context: Ctx, params: any, user: User | null, _req: void, raw: express.Request): Promise<void> => {
                return handler(context, params, user, raw);
            };

            this.genericHandler<void, void>("DELETE", url, undefined, undefined, req, res, handlerWrapper);
        });
    }

    GET_streaming(
        url: string,
        doc: string,
        handler: (
            context: Ctx,
            params: any,
            user: User | null,
            respond: (writer: (write: (data: string) => Promise<void>) => Promise<void>) => Promise<ResponseReceived>
        ) => Promise<ResponseReceived>): void {
        // Ignore parameter
        doc; // tslint:disable-line:no-unused-expression

        const method = "GET";
        this.app.get(url, async (req, res) => {
            res.header("X-MedFlyt-Route", url);

            let user: User | null = null;

            let ctx: CtxImpl;
            try {
                ctx = this.requestContextCallbacks.onRequestBegin(method, url, req);
            } catch (e) {
                this.sendServerErrorResponse(req, res, user, e);
                logger.commError(req, res, user, "500 Internal Server Error");
                return;
            }

            let sentHeaders = false;

            try {
                const authResult = await this.auth(ctx, req);
                if (authResult === NoAuth.EXPIRED) {
                    res.status(401).contentType("application/json").send(JSON.stringify({ "error": "AuthToken invalid or expired" }));
                    logger.commError(req, res, null, { error: "AuthToken invalid or expired" });
                    this.requestContextCallbacks.onRequestEnd(ctx, res.statusCode);
                    return;
                } else if (authResult === NoAuth.ANONYMOUS) {
                    user = null;
                } else if (authResult === NoAuth.BLOCKED) {
                    res.status(423).contentType("application/json").send(JSON.stringify({ "error": "User is blocked" }));
                    logger.commError(req, res, null, { error: "User is blocked" });
                    this.requestContextCallbacks.onRequestEnd(ctx, res.statusCode);
                    return;
                } else if (authResult === NoAuth.IP_ADDRESS_RESTRICTED) {
                    res.status(401).contentType("application/json").send(JSON.stringify({ "error": "IP address is not allowed" }));
                    logger.commError(req, res, null, { error: "IP address is not allowed" });
                    this.requestContextCallbacks.onRequestEnd(ctx, res.statusCode);
                    return;
                } else {
                    user = authResult;
                }

                const respond = async (writer: (write: (data: string) => Promise<void>) => Promise<void>): Promise<ResponseReceived> => {
                    res.status(200).contentType("application/json").setHeader("Transfer-Encoding", "chunked");
                    sentHeaders = true;

                    const write = (data: string): Promise<void> => {
                        if (data === "") {
                            return Promise.resolve();
                        }
                        return new Promise<void>((resolve, _reject) => {
                            res.write(data, () => {
                                // Intentionally ignore errors. If there is an
                                // error sending, then that is not the fault of
                                // the endpoint.
                                resolve();
                            });
                        });
                    };

                    await writer(write);

                    res.end();
                    return null as any as ResponseReceived; // The value of "ResponseReceived" is not important, it is just to satifsy the type checker.
                };

                try {
                    await handler(ctx, req.params, user, respond);
                } catch (e) {
                    if (!sentHeaders && e instanceof StatusError) {
                        if (typeof e.body === "object") {
                            res.status(e.statusCode).contentType("application/json").send(JSON.stringify(e.body));
                            logger.commError(req, res, user, e.body);
                        } else {
                            res.status(e.statusCode).send();
                            logger.commError(req, res, user, "");
                        }
                        this.requestContextCallbacks.onRequestEnd(ctx, e.statusCode);
                        return;
                    } else {
                        throw e;
                    }
                }
                this.requestContextCallbacks.onRequestEnd(ctx, res.statusCode);
            } catch (e) {
                if (sentHeaders) {
                    this.errorCallback(req, user, e);
                }
                this.requestContextCallbacks.onRequestEnd(ctx, res.statusCode);

                if (!sentHeaders) {
                    this.sendServerErrorResponse(req, res, user, e);
                    logger.commError(req, res, user, "500 Internal Server Error");
                } else {
                    // There was an error while the response body was already
                    // streaming out. There is no clean way to handle this
                    res.end("<Internal Server Error>");
                }
            }
        });
    }

    private genericHandler<Req, Rsp>(
        method: string,
        url: string,
        reqType: MessageName | void,
        rspType: MessageName | void,
        req: express.Request,
        res: express.Response,
        handler: (context: Ctx, params: any, user: User | null, req: Req, raw: express.Request) => Promise<Rsp>): void {
        // tslint:disable-next-line:no-floating-promises
        this.genericHandlerImpl(method, url, reqType, rspType, req, res, handler);
    }

    private async genericHandlerImpl<Req, Rsp>(
        method: string,
        url: string,
        reqType: MessageName | void,
        rspType: MessageName | void,
        req: express.Request,
        res: express.Response,
        handler: (context: Ctx, params: any, user: User | null, req: Req, raw: express.Request) => Promise<Rsp>): Promise<void> {
        res.header("X-MedFlyt-Route", url);

        let user: User | null = null;

        let ctx: CtxImpl;
        try {
            ctx = this.requestContextCallbacks.onRequestBegin(method, url, req);
        } catch (e) {
            this.sendServerErrorResponse(req, res, user, e);
            logger.commError(req, res, user, "500 Internal Server Error");
            return;
        }

        try {

            if (reqType === undefined) {
                // TODO validate empty request body?
            } else {
                const validate = messageValidator(reqType);
                if (validate(req.body) !== true) {
                    res.status(400).contentType("application/json").send(JSON.stringify(validate.errors));
                    logger.commError(req, res, null, validate.errors);
                    this.requestContextCallbacks.onRequestEnd(ctx, res.statusCode);
                    return;
                }
            }

            const authResult = await this.auth(ctx, req);
            if (authResult === NoAuth.EXPIRED) {
                res.status(401).contentType("application/json").send(JSON.stringify({ "error": "AuthToken invalid or expired" }));
                logger.commError(req, res, null, { error: "AuthToken invalid or expired" });
                this.requestContextCallbacks.onRequestEnd(ctx, res.statusCode);
                return;
            } else if (authResult === NoAuth.ANONYMOUS) {
                user = null;
            } else if (authResult === NoAuth.BLOCKED) {
                res.status(423).contentType("application/json").send(JSON.stringify({ "error": "User is blocked" }));
                logger.commError(req, res, null, { error: "User is blocked" });
                this.requestContextCallbacks.onRequestEnd(ctx, res.statusCode);
                return;
            } else if (authResult === NoAuth.IP_ADDRESS_RESTRICTED) {
                res.status(401).contentType("application/json").send(JSON.stringify({ "error": "IP address is not allowed" }));
                logger.commError(req, res, null, { error: "IP address is not allowed" });
                this.requestContextCallbacks.onRequestEnd(ctx, res.statusCode);
                return;
            } else {
                user = authResult;
            }

            let response: Rsp;
            try {
                response = await handler(ctx, req.params, user, req.body, req);
            } catch (e) {
                if (e instanceof StatusError) {
                    if (typeof e.body === "object") {
                        res.status(e.statusCode).contentType("application/json").send(JSON.stringify(e.body));
                        logger.commError(req, res, user, e.body);
                    } else {
                        res.status(e.statusCode).send();
                        logger.commError(req, res, user, "");
                    }
                    this.requestContextCallbacks.onRequestEnd(ctx, e.statusCode);
                    return;
                } else if (e instanceof ReqBodyValidationError) {
                    res.status(400).contentType("application/json").send(JSON.stringify(e.errors));
                    logger.commError(req, res, user, e.errors);
                    this.requestContextCallbacks.onRequestEnd(ctx, res.statusCode);
                    return;
                } else {
                    throw e;
                }
            }
            if (rspType === undefined) {
                if (response !== undefined) {
                    const errMsg = `Handler was expected to return void, but returned: ${response}`;
                    logger.error(`Handler was expected to return void, but returned: ${response}`);
                    throw new Error(errMsg);
                }
                res.status(204).send();
                logger.commInfo(req, res, user, "");
            } else {
                const jsonResponse = JSON.stringify(response);
                if (this.debugValidateResponses) {
                    const validate = messageValidator(rspType);
                    if (validate(JSON.parse(jsonResponse)) !== true) {
                        const errMsg = "Invalid response built for " + req.url + "\n" + JSON.stringify(validate.errors) + "\n" + jsonResponse.substr(0, 2048);
                        logger.error(errMsg);
                        throw new Error(errMsg);
                    }
                }
                res.contentType("application/json").send(jsonResponse);
                logger.commInfo(req, res, user, response);
            }
            this.requestContextCallbacks.onRequestEnd(ctx, res.statusCode);
        } catch (e) {
            this.sendServerErrorResponse(req, res, user, e);
            logger.commError(req, res, user, "500 Internal Server Error");
            this.requestContextCallbacks.onRequestEnd(ctx, res.statusCode);
        }
    }
}