import * as messages from "../../../Messages";
import { NumberConverter } from "../../../Utils/Numbers";
import { Instant } from 'js-joda'
import pjson from '../../../../package.json';

const categoriesMokeArr = ["search_engines", "engineering", "porngraphy", "hatespeech"];
const domainsMokeArr = ["www.google.co.il", "starve-yourself.org", "big-balagan.com"];

export function getProbabilisticFilters(makerVersion: string, categories: string[]): Promise<messages.ProbabilisticsFilters> {
    const probsFilter: messages.ProbabilisticFilters[] = [];
    return new Promise<messages.ProbabilisticsFilters>((resolve, rejects) => {

        for (let i = 0; i < 10; i++) {
            probsFilter.push({
                id: (i + 41 * 80 / 45).toString(),
                category: categoriesMokeArr[NumberConverter.getRandomIntBetweenTwoValues(0, categoriesMokeArr.length - 1)],
                size: NumberConverter.getRandomIntBetweenTwoValues(200, i * 1000),
                filepath: "social_networks-8bit",
                bpe: NumberConverter.getRandomIntBetweenTwoValues(40, i * 1000),
                categoryIndex: NumberConverter.getRandomIntBetweenTwoValues(40, i * 1000),
                createAt: Instant.now(),
                updateAt: Instant.now(),
                makerVersion: pjson.version // here the version from keeper-api package.json

            });
        }

        const filterObj: messages.ProbabilisticsFilters = {
            probalisticFilters: probsFilter

        }
        resolve(filterObj);
    })
}



export function getIncrementalUpdate(categoryIndex: number, categories: string[]): Promise<messages.CateIdxToDomainsCategories> {
    const filterObj: messages.CateIdxToDomainsCategories = {
        categoryIndex,
        domainsCategories: []
    }
    return new Promise<messages.CateIdxToDomainsCategories>((resolve, rejects) => {

        for (let i = 0; i < 10; i++) {
            filterObj.domainsCategories.push({
                domain: domainsMokeArr[NumberConverter.getRandomIntBetweenTwoValues(0, domainsMokeArr.length - 1)],
                category: categoriesMokeArr[NumberConverter.getRandomIntBetweenTwoValues(0, categoriesMokeArr.length - 1)]
            });
        }


        resolve(filterObj);
    })
}