import { Router } from "express";
import * as messages from "../../../Messages"
import { getParamAsString, getStringArrayComa, getParamAsInt } from "../../../Utils/QueryParams";
import { getProbabilisticFilters, getIncrementalUpdate } from "../controller/filters_ctrl";


export function addRoutes(router: Router) {
    router.get(
        "/probabilistic-filters", // :makerVersion/:categories",
        async (req: any, res: any) => {
            const makerVersion = getParamAsString(req.params, "makerVersion");
            const categories = getStringArrayComa(req.params, "categories");
            const probFilters = await new Promise<messages.ProbabilisticsFilters>((resolve, rejects) => {
                resolve(getProbabilisticFilters(makerVersion, categories));
            });

            res.json(probFilters.probalisticFilters);
        })

    router.get(
        "/incremental-updates",/// :categoryIndex/:categories
        async (req: any, res: any) => {
            const categoryIndex = getParamAsInt(req.params, "categoryIndex");
            const categories = getStringArrayComa(req.params, "categories");
            const probFilters = await new Promise<messages.CateIdxToDomainsCategories>((resolve, rejects) => {
                resolve(getIncrementalUpdate(categoryIndex, categories));
            });

            res.json(probFilters);
        })
}