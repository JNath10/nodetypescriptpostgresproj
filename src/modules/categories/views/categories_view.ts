import { Router } from 'express';
import * as messages from "../../../Messages"
import { getParamAsString, getParamAsStringArray } from "../../../Utils/QueryParams";
import { getCategories, getDomainsCategoriesBulk, getIpsCategoriesBulk } from "../controller/categories_ctrl";



export function addRoutes(router: Router) {
    router.get(
        "/domain-categories/:domain",
        async (req: any, res: any) => {

            const domain = getParamAsString(req.params, "domain");
            const categories = await new Promise<messages.Categories>((resolve, rejects) => {
                resolve(getCategories());
            });

            res.json(categories);
        })

    router.get(
        "/ip-categories/:ip",
        async (req: any, res: any) => {

            const ip = getParamAsString(req.params, "ip");
            const categories = await new Promise<messages.Categories>((resolve, rejects) => {
                resolve(getCategories());
            });

            res.json(categories);
        })

    router.post(
        "/domain-categories",
        async (req: any, res: any) => {

            const domains : string[] = getParamAsStringArray(req.body);

            const domainCategoryBulk = await new Promise<messages.DomainsCategoriesBulk>((resolve, rejects) => {
                resolve(getDomainsCategoriesBulk(domains));
            });

            res.json(domainCategoryBulk);
        })

        router.post(
            "/ip-categories",
            async (req: any, res: any) => {

                const ips : string[] = getParamAsStringArray(req.body);

                const ipToCategoriesBulk = await new Promise<messages.IpToCategoriesBulk>((resolve, rejects) => {
                    resolve(getIpsCategoriesBulk(req.categories));
                });

                res.json(ipToCategoriesBulk);
            })
}
