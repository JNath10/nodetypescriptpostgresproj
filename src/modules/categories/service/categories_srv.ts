import axios from "axios";
import { getRandomDataFromArr, removeDuplicateValueFromArr } from "../../../Utils/DataManipulation";
import { Connection } from "../../../mfsTools/mfsqltool";
import { SingleConnection } from "../../../singelConnection";


let categoriesMokeArr = ["search_engines", "engineering", "porngraphy", "hatespeech"];
const domainsMokeArr = ["www.google.co.il", "starve-yourself.org", "big-balagan.com"];
export async function setMochData(conn: Connection<{}>) {
    categoriesMokeArr = [];
    const rows = await conn.query(conn.sql`select * from catergory`);
    for (const row of rows) {
        categoriesMokeArr.push(row.name.val());
    }
}
export async function getCategories(): Promise<string[]> {
    return new Promise<string[]>((resolve, reject) => {
        const conn = SingleConnection.getConnection();
        conn.query(conn.sql`select name from catergory`).then(rows => {
            const resArr = rows.map(x => x.name.val());
            const arr: string[] = [];
            for (const val of resArr) {
                arr.push(getRandomDataFromArr(resArr) as string);
            }
            resolve(removeDuplicateValueFromArr(arr));
        });

    })
}


