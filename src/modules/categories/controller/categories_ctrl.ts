import * as messages from "../../../Messages";
import * as ctgSrv from "../service/categories_srv";
// import * as test from "../../../demo"
export function getCategories(): Promise<messages.Categories> {
    const categs: messages.Categories = {
        categories: []
    };
    return new Promise<messages.Categories>((resolve, rejects) => {
        ctgSrv.getCategories().then(vals => {
            categs.categories = [];
            for (const val of vals) {
                categs.categories.push(val);
            }
            resolve(categs);
        });

    })
}

export function getDomainsCategoriesBulk(domains: string[]): Promise<messages.DomainsCategoriesBulk> {
    const categs: messages.DomainsCategoriesBulk = {
        domainToCategories: []
    };
    return new Promise<messages.DomainsCategoriesBulk>((resolve, rejects) => {
        categs.domainToCategories = [];
        categs.domainToCategories.push({
            catergories: ["gsdfg", "af"],
            domain: "www.google.com"
        });
        resolve(categs);
    })
}


export function getIpsCategoriesBulk(domains: string[]): Promise<messages.IpToCategoriesBulk> {
    const categs: messages.IpToCategoriesBulk = {
        ipsToCategories: []
    };
    return new Promise<messages.IpToCategoriesBulk>((resolve, rejects) => {
        categs.ipsToCategories = [];
        categs.ipsToCategories.push({
            ip: "8.8.5.5",
            categories: ["gsdfg", "af"]
        });
        resolve(categs);
    })
}