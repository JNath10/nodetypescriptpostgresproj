import * as pg from "pg";
import { defineSqlView, Connection, Req, Opt, migrateDatabase, sqlFrag } from "./mfsTools/mfsqltool";
import { CategoryId } from "./types";
import { Categories } from "./Messages";


export class SingleConnection {
    private static instance: SingleConnection;
    private static connection: Connection<{}>;
    private constructor() {
        this.getDbConnectionInstance().then(conn=>{
            SingleConnection.connection = conn;
        })
    }

    public static getInstance(): SingleConnection {
        if (!SingleConnection.instance) {
            SingleConnection.instance = new SingleConnection();
        }

        return SingleConnection.instance;
    }
    public connect() {
        this.getDbConnectionInstance().then(conn=>{
            SingleConnection.connection = conn;
        })
    }
    public static getConnection(): Connection<{}>{
        return this.connection;
    }


    async getDbConnectionInstance(): Promise<Connection<{}>> {
        let connection: Connection<{}>;
        console.log("Connecting...");
        const config: pg.ClientConfig = {
            host: process.env.DB_HOST,
            port: parseInt(process.env.DB_PORT, 10),
            password: process.env.DB_PASSWORD,
            database: process.env.DB_NAME,
            user: process.env.DB_USER
        }
        const client = await connectPg(config);
        try {
            console.log("Migrating...");
            await migrateDatabase(client, "migrations", logger);
            console.log("Done");

            connection = new Connection(client);
            // const rows = await connection.query(connection.sql`select * from catergory`);
            // for (const row of rows) {
            //     console.log(row.name.val());
            // }

        } finally {
            // await closePg(client);
        }
        return connection;
    }

}



export async function test() {
    const conn: Connection<CategoryId | CategoryId[]> = null as any;
    const rows = await conn.query<{

    }>(conn.sql
        ` SELECT * FROM catergory
        `);


    console.log(rows[0]);


}

function connectPg(config: pg.ClientConfig): Promise<pg.Client> {
    const client = new pg.Client(config);
    return new Promise<pg.Client>((resolve, reject) => {
        client.connect(err => {
            if ((err as any) as boolean) {
                reject(err);
                return;
            }
            resolve(client);
        });
    });
}

function closePg(conn: pg.Client): Promise<void> {
    return new Promise<void>((resolve, reject) => {
        conn.end(err => {
            if ((err as any) as boolean) {
                reject(err);
                return;
            }
            resolve();
        });
    });
}

async function logger(msg: string): Promise<void> {
    console.log(msg);
}



