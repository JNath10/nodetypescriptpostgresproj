export interface Categories {
    categories: string[];
}

export interface DomainsCategoriesBulk{
    domainToCategories: DomainToCategories[];
}
export interface DomainToCategories {
    domain: string;
    catergories: string[]
}

export interface IpToCategoriesBulk {
    ipsToCategories: IpToCategories[];
}

export interface IpToCategories {
    ip: string;
    categories: string[]
}