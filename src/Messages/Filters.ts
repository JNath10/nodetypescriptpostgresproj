
import { Instant } from 'js-joda'

export interface ProbabilisticsFilters {
    probalisticFilters: ProbabilisticFilters[];
}


export interface ProbabilisticFilters {
    id: string;
    category: string;
    filepath: string;
    bpe: number;
    size: number;
    categoryIndex: number;
    createAt: Instant;
    updateAt: Instant;
    makerVersion: string;
}


export interface CateIdxToDomainsCategories {
    categoryIndex: number;
    domainsCategories: DomainToCategory[];
}

export interface DomainToCategory {
    domain: string;
    category: string;
}
