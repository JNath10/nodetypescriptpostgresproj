/* tslint:disable:max-classes-per-file */

export class CategoryId {
    static wrap(val: number): CategoryId {
        return val as any;
    }

    static unwrap(val: CategoryId): number {
        return val as any;
    }

    protected _dummy: CategoryId[];
}