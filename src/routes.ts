import * as categoriesView from "./modules/categories/views/categories_view";
import * as filtersView from "./modules/filters/views/filters_view";
import {Router} from "express";

export function addRoutes(router: Router) {
    categoriesView.addRoutes(router);
    filtersView.addRoutes(router);
}